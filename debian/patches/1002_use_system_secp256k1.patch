Description: Build against system secp256k1 when available
Author: Jonas Smedegaard <dr@jones.dk>
License: Expat
Last-Update: 2017-07-09
---
This patch header follows DEP-3: http://dep.debian.net/deps/dep3/
--- a/configure.ac
+++ b/configure.ac
@@ -262,6 +262,12 @@
   AC_DEFINE(USE_ASM, 1, [Define this symbol to build in assembly routines])
 fi
 
+AC_ARG_WITH([system-secp256k1],
+  [AS_HELP_STRING([--with-system-secp256k1],
+  [Build with system secp256k1 (default is no)])],
+  [system_secp256k1=$withval],
+  [system_secp256k1=no])
+
 AC_ARG_WITH([system-univalue],
   [AS_HELP_STRING([--with-system-univalue],
   [Build with system UniValue (default is no)])],
@@ -1480,6 +1486,44 @@
   fi
 fi
 
+dnl secp256k1 check
+
+if test x$system_secp256k1 != xno ; then
+  found_secp256k1=no
+  if test x$use_pkgconfig = xyes; then
+    : #NOP
+    m4_ifdef(
+      [PKG_CHECK_MODULES],
+      [
+        PKG_CHECK_MODULES([SECP256K1],[libsecp256k1],[found_secp256k1=yes],[true])
+      ]
+    )
+  else
+    AC_CHECK_HEADERS([secp256k1.h secp256k1_recovery.h],[
+      AC_CHECK_LIB([secp256k1],  [main],[
+        SECP256K1_LIBS=-lsecp256k1
+        found_secp256k1=yes
+      ],[true])
+    ],[true])
+  fi
+
+  if test x$found_secp256k1 = xyes ; then
+    system_secp256k1=yes
+  elif test x$system_secp256k1 = xyes ; then
+    AC_MSG_ERROR([secp256k1 not found])
+  else
+    system_secp256k1=no
+  fi
+fi
+
+if test x$system_secp256k1 = xno ; then
+  SECP256K1_CFLAGS='-I$(srcdir)/secp256k1/include'
+  SECP256K1_LIBS='secp256k1/libsecp256k1.la'
+fi
+AM_CONDITIONAL([EMBEDDED_SECP256K1],[test x$system_secp256k1 = xno])
+AC_SUBST(SECP256K1_CFLAGS)
+AC_SUBST(SECP256K1_LIBS)
+
 dnl libevent check
 
 if test x$build_bitcoin_cli$build_bitcoind$bitcoin_enable_qt$use_tests$use_bench != xnonononono; then
@@ -1914,7 +1958,9 @@
 fi
 
 ac_configure_args="${ac_configure_args} --disable-shared --with-pic --enable-benchmark=no --enable-module-recovery --enable-module-schnorrsig --enable-experimental"
-AC_CONFIG_SUBDIRS([src/secp256k1])
+if test x$system_secp256k1 = xno; then
+  AC_CONFIG_SUBDIRS([src/secp256k1])
+fi
 
 AC_OUTPUT
 
--- a/src/Makefile.am
+++ b/src/Makefile.am
@@ -15,6 +15,15 @@
 PTHREAD_FLAGS = $(PTHREAD_CFLAGS) $(PTHREAD_LIBS)
 EXTRA_LIBRARIES =
 
+if EMBEDDED_SECP256K1
+LIBSECP256K1 = secp256k1/libsecp256k1.la
+
+$(LIBSECP256K1): $(wildcard secp256k1/lib/*) $(wildcard secp256k1/include/*)
+	$(AM_V_at)$(MAKE) $(AM_MAKEFLAGS) -C $(@D) $(@F)
+else
+LIBSECP256K1 = $(SECP256K1_LIBS)
+endif
+
 if EMBEDDED_UNIVALUE
 LIBUNIVALUE = univalue/libunivalue.la
 
@@ -24,7 +33,7 @@
 LIBUNIVALUE = $(UNIVALUE_LIBS)
 endif
 
-BITCOIN_INCLUDES=-I$(builddir) -I$(srcdir)/secp256k1/include $(BDB_CPPFLAGS) $(BOOST_CPPFLAGS) $(LEVELDB_CPPFLAGS)
+BITCOIN_INCLUDES=-I$(builddir) $(SECP256K1_CFLAGS) $(BDB_CPPFLAGS) $(BOOST_CPPFLAGS) $(LEVELDB_CPPFLAGS)
 
 BITCOIN_INCLUDES += $(UNIVALUE_CFLAGS)
 
@@ -35,7 +44,6 @@
 LIBBITCOIN_UTIL=libbitcoin_util.a
 LIBBITCOIN_CRYPTO_BASE=crypto/libbitcoin_crypto_base.a
 LIBBITCOINQT=qt/libbitcoinqt.a
-LIBSECP256K1=secp256k1/libsecp256k1.la
 
 if ENABLE_ZMQ
 LIBBITCOIN_ZMQ=libbitcoin_zmq.a
@@ -62,9 +70,6 @@
 LIBBITCOIN_CRYPTO += $(LIBBITCOIN_CRYPTO_SHANI)
 endif
 
-$(LIBSECP256K1): $(wildcard secp256k1/src/*.h) $(wildcard secp256k1/src/*.c) $(wildcard secp256k1/include/*)
-	$(AM_V_at)$(MAKE) $(AM_MAKEFLAGS) -C $(@D) $(@F)
-
 # Make is not made aware of per-object dependencies to avoid limiting building parallelization
 # But to build the less dependent modules first, we manually select their order here:
 EXTRA_LIBRARIES += \
@@ -755,7 +760,7 @@
 
 libbitcoinconsensus_la_LDFLAGS = $(AM_LDFLAGS) -no-undefined $(RELDFLAGS)
 libbitcoinconsensus_la_LIBADD = $(LIBSECP256K1)
-libbitcoinconsensus_la_CPPFLAGS = $(AM_CPPFLAGS) -I$(builddir)/obj -I$(srcdir)/secp256k1/include -DBUILD_BITCOIN_INTERNAL
+libbitcoinconsensus_la_CPPFLAGS = $(AM_CPPFLAGS) -I$(builddir)/obj $(SECP256K1_CFLAGS)
 libbitcoinconsensus_la_CXXFLAGS = $(AM_CXXFLAGS) $(PIE_FLAGS)
 
 endif
--- a/src/Makefile.test.include
+++ b/src/Makefile.test.include
@@ -347,7 +347,9 @@
 	$(BENCH_BINARY) > /dev/null
 endif
 endif
+if EMBEDDED_SECP256K1
 	$(AM_V_at)$(MAKE) $(AM_MAKEFLAGS) -C secp256k1 check
+endif
 if EMBEDDED_UNIVALUE
 	$(AM_V_at)$(MAKE) $(AM_MAKEFLAGS) -C univalue check
 endif
